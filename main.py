from __future__ import annotations
from dataclasses import dataclass
from pathlib import Path
from typing import Callable

import cv2 as cv
import numpy as np
import easygui
import pymsgbox

from shrink import shrink_to_fit


@dataclass
class Model:
    on_values_changed: Callable[[Model], None]

    sure_foreground_threshold: float = 0.7

    def set_sure_foreground_threshold(self, value_times_100: int) -> None:
        self.sure_foreground_threshold = 0.01 * max(1, value_times_100)
        self.on_values_changed(self)


def apply_watershed(gray: np.ndarray, model: Model, target: np.ndarray) -> np.ndarray:
    # gray = cv.blur(gray, (3, 3))
    ret, thresh = cv.threshold(gray, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)

    kernel = np.ones((3, 3), np.uint8)
    opening = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel, iterations=2)

    # sure background area
    sure_bg = cv.dilate(opening, kernel, iterations=3)

    # Finding sure foreground area
    dist_transform = cv.distanceTransform(opening, cv.DIST_L2, 5)
    ret, sure_fg = cv.threshold(dist_transform, model.sure_foreground_threshold * dist_transform.max(), 255, 0)

    # Finding unknown region
    sure_fg = np.uint8(sure_fg)
    unknown = cv.subtract(sure_bg, sure_fg)

    # Marker labelling
    ret, markers = cv.connectedComponents(sure_fg)
    # Add one to all labels so that sure background is not 0, but 1
    markers = markers + 1
    # Now, mark the region of unknown with zero
    markers[unknown == 255] = 0

    markers = cv.watershed(target, markers)
    target[markers == -1] = (255, 64, 64)

    return target


def select_image(prompt: str) -> np.ndarray:
    if (path_str := easygui.fileopenbox(title=prompt)) is None:
        exit(0)

    file_path = Path(path_str)

    if (image := cv.imread(str(file_path))) is None:
        pymsgbox.alert(title='Error', text=f'Could not open file:\n{file_path}', icon=pymsgbox.WARNING)
        exit(1)

    return image


def main() -> None:
    image = select_image('Open image')
    image = shrink_to_fit(image, (400, 400))
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

    window_name = 'Main'
    cv.namedWindow(window_name, cv.WINDOW_GUI_EXPANDED)
    cv.resizeWindow(window_name, image.shape[1], image.shape[0])

    sliders_window_name = 'Sliders'
    cv.namedWindow(sliders_window_name, cv.WINDOW_GUI_EXPANDED)

    def redraw(model_: Model) -> None:
        cv.imshow(window_name, apply_watershed(gray, model_, image.copy()))

    model = Model(redraw)

    cv.createTrackbar(
        'sure fg threshold', sliders_window_name,
        int(100 * model.sure_foreground_threshold), 100, model.set_sure_foreground_threshold
    )

    cv.waitKey(0)
    cv.destroyAllWindows()


if __name__ == '__main__':
    main()
